package com.example.drug.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;

import com.example.drug.R;
import com.example.drug.utils.Condition;
import com.example.drug.view.activity.ScanCodeActivity;
import com.example.drug.view.activity.medicine_cabinet;
import com.example.drug.view.activity.tips_activity;

public class Fragment_Home extends Fragment implements View.OnClickListener {

    private View mView;

    private ViewFlipper mFlipper;

    private RelativeLayout mLayoutScanCode;
    private RelativeLayout mLayoutDrug;
    private  RelativeLayout layout_drugs;
    private  RelativeLayout layout_4;
    private EditText mEdtSearchhome;

    private OnChangeFragment mOnChangeFragment;

    private ImageView mImgScan;

    @Override
    public void onAttach(Activity activity) {
        mOnChangeFragment = (OnChangeFragment) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    //Khởi tạo ban đầu
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        mFlipper = mView.findViewById(R.id.viewFilper);
        mLayoutScanCode = mView.findViewById(R.id.layout_scan_code);
        mLayoutScanCode.setOnClickListener(this);
        mLayoutDrug = mView.findViewById(R.id.layout_drug_home);
        mLayoutDrug.setOnClickListener(this);
        mEdtSearchhome = mView.findViewById(R.id.edt_search_home);
        mEdtSearchhome.setOnClickListener(this);
        mImgScan = mView.findViewById(R.id.img_scan_home);
        layout_drugs = mView.findViewById(R.id.layout_drugs);
        layout_4 = mView.findViewById(R.id.layout_4);
        layout_4.setOnClickListener(this);
        mImgScan.setOnClickListener(this);
        layout_drugs.setOnClickListener(this);
        return mView;
    }

    //khởi tạo kết show silde ảnh ở màn hình chính
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        for (int i = 0; i < Condition.IMAGE.length; i++) {
            //  This will create dynamic image view and add them to ViewFlipper
            setFlipperImage(Condition.IMAGE[i]);
        }
        super.onActivityCreated(savedInstanceState);
    }
    //set ảnh show ở màn hình
    private void setFlipperImage(int res) {
        ImageView image = new ImageView(getContext());
        image.setBackgroundResource(res);
        mFlipper.addView(image);
        mFlipper.setFlipInterval(Condition.TIME_TRANLATE_IMAGE);
        mFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.out_from_left));
        mFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.in_from_right));
        mFlipper.setAutoStart(true);
        mFlipper.startFlipping();
    }
//sự kiện click các chức năng
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_scan_code://chuyển qua màn hình scan barcode
                startActivity(new Intent(getContext(), ScanCodeActivity.class));
                break;
            case R.id.layout_drug_home: // chuyển qua màn hình tìm kiếm thuốc
                mOnChangeFragment.setOnChangeFragment();
                break;
            case R.id.edt_search_home:
                mOnChangeFragment.setOnChangeFragment();
                break;
            case R.id.img_scan_home://chuyển qua màn hình scan barcode
                startActivity(new Intent(getContext(), ScanCodeActivity.class));
                break;
            case R.id.layout_drugs://chuỷen qua màn hình tủ thuốc
                startActivity(new Intent(getContext(), medicine_cabinet.class));
                break;
            case R.id.layout_4:
                startActivity(new Intent(getContext(), tips_activity.class));
                break;
        }
    }
}
