package com.example.drug.view.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.drug.DataBase.DataBase;
import com.example.drug.R;
import com.example.drug.adapter.DrugAdapter;
import com.example.drug.model.Drug;
import com.example.drug.utils.Condition;
import com.example.drug.view.activity.InforDrugActivity;
import com.example.drug.view.activity.MainActivity;
import com.example.drug.view.activity.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

public class Fragment_Drug extends Fragment implements View.OnClickListener, TextWatcher, OnItemClickListener {
    private View mView;

    private EditText mEdtSearch;

    private RecyclerView mRecycle;

    private ImageView mImgReset;

    private DrugAdapter mAdapter;

    private List<Drug> mList;

    private SQLiteDatabase mDb;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mDb = DataBase.initDatabase(getContext(), Condition.DATABASE_NAME);//khởi tại kết nối db
    }

    @Override
    public void onResume() {
        this.mList = MainActivity.mList;
        mAdapter = new DrugAdapter(getContext(), mList);
        mRecycle.setAdapter(mAdapter);
        mRecycle.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter.notifyDataSetChanged();
        mAdapter.setOnItemClickListener(this);
        searchDrug(getText());
        super.onResume();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_drug, container, false);
        mEdtSearch = mView.findViewById(R.id.edt_search);
        mEdtSearch.addTextChangedListener(this);
        mImgReset = mView.findViewById(R.id.img_reset_search);
        mImgReset.setOnClickListener(this);

        mRecycle = mView.findViewById(R.id.recycleDrug);

        return mView;
    }

    //sự kiện click
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_reset_search:
                mEdtSearch.setText("");
                break;
        }
    }

    private String getText() {
        return mEdtSearch.getText().toString().trim();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        mImgReset.setVisibility(getText().isEmpty() ? View.GONE : View.VISIBLE);
        searchDrug(getText());
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
    //tìm kiếm thuốc theo mã tên và laoij
    private void searchDrug(final String value) {
        this.mList.clear();
        Cursor cursor = mDb.rawQuery("Select * From drugs where code like '" + value + "%' or name_drug like'"+ value +"%' or assign_medication like'%"+value+"%'", null);
        if (cursor.moveToFirst()) {
            do {
                mList.add(new Drug(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11),
                        cursor.getString(12),
                        cursor.getString(13),
                        cursor.getString(14),
                        cursor.getString(15),
                        cursor.getString(16),
                        cursor.getString(17),
                        cursor.getString(18)));
            } while (cursor.moveToNext());
        }
    }

    //click chọn 1 cía thuốc để xem chi tiết
    @Override
    public void setOnItemClickListener(int i) {
        Intent intent = new Intent(getContext(), InforDrugActivity.class);
        intent.putExtra("data", mList.get(i));
        getActivity().startActivity(intent);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
