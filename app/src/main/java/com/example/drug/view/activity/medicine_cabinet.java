package com.example.drug.view.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.example.drug.DataBase.DataBase;
import com.example.drug.R;
import com.example.drug.adapter.DrugAdapter;
import com.example.drug.model.Drug;
import com.example.drug.utils.Condition;

import java.util.ArrayList;
import java.util.List;

public class medicine_cabinet extends AppCompatActivity implements OnItemClickListener {
    private Toolbar mToolbar;
    private RecyclerView mRecycle;

    private ImageView mImgReset;

    private DrugAdapter mAdapter;

    private List<Drug> mList;

    private SQLiteDatabase mDb;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medicine_cabinet);
        init();
    }
    //khai báo dữ liệu ban đầu
    public void init()
    {
        this.mDb = DataBase.initDatabase(this, Condition.DATABASE_NAME);
        mToolbar = findViewById(R.id.toolbar_infor);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("Tủ thuốc");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        mRecycle = findViewById(R.id.ry);
        this.mList = MainActivity.mList;
        mAdapter = new DrugAdapter(this, mList);
        mRecycle.setAdapter(mAdapter);
        mRecycle.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mAdapter.notifyDataSetChanged();
        mAdapter.setOnItemClickListener(this);
    }
    //phương thức nhận biết khi màn hình này được mở lại
    @Override
    public void onResume() {
        super.onResume();
        searchAdd();
    }
    //sự kiện click phím quay lại
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        //click vào thêm dữ liệu vào bảng
        return super.onOptionsItemSelected(item);
    }
    //click list để xem chi tiết thuốc
    @Override
    public void setOnItemClickListener(int i) {
        Intent intent = new Intent(this, InforDrugActivity.class);
        intent.putExtra("data", searchDrug(mList.get(i).getCode()).get(0));
        startActivity(intent);
    }
    //lấy ra list tủ thuốc
    private void searchAdd() {
        this.mList.clear();
        Cursor cursor = mDb.rawQuery("Select * From tbl_medicine", null);
        if (cursor.moveToFirst()) {
            do {
                mList.add(new Drug(cursor.getString(0),
                        cursor.getString(1),
                       "",
                        "",
                        "",
                        cursor.getString(2),
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        ""));
            } while (cursor.moveToNext());
            Log.e("mList",mList.get(0).getImage());
        }
    }
    //lấy ra 1 thuốc theo tên hoăc mã
    private List<Drug> searchDrug(final String value) {
        List<Drug> arr = new ArrayList<>();
        Cursor cursor = mDb.rawQuery("Select * From drugs where code like '" + value + "%' or name_drug like'"+ value +"%' or assign_medication like'%"+value+"%'", null);
        if (cursor.moveToFirst()) {
            do {
                arr.add(new Drug(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11),
                        cursor.getString(12),
                        cursor.getString(13),
                        cursor.getString(14),
                        cursor.getString(15),
                        cursor.getString(16),
                        cursor.getString(17),
                        cursor.getString(18)));
            } while (cursor.moveToNext());

        }
        return arr;
    }
}
