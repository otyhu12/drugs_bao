package com.example.drug.view.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;

import com.example.drug.DataBase.DataBase;
import com.example.drug.R;
import com.example.drug.adapter.DrugAdapter;
import com.example.drug.adapter.tips_adapter;
import com.example.drug.model.Drug;
import com.example.drug.model.tips;
import com.example.drug.utils.Condition;

import java.util.ArrayList;
import java.util.List;

public class tips_activity extends AppCompatActivity  implements View.OnClickListener, OnItemClickListener {
    private RecyclerView mRecycle;
    private Toolbar mToolbar;
    public Menu menu;
    private SQLiteDatabase mDb;
    private tips_adapter mAdapter;

    private List<tips> mList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tips_activity);
        init();

    }
    public void init()
    {
        mList = new ArrayList<>();
        mRecycle = findViewById(R.id.recycle_tip);
        this.mDb = DataBase.initDatabase(this, Condition.DATABASE_NAME);//khởi tạo kết nối sqlite
        mToolbar = findViewById(R.id.toolbar_infor);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("Chăm sóc");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        mAdapter = new tips_adapter(getBaseContext(), mList);
        mRecycle.setAdapter(mAdapter);
        mRecycle.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mAdapter.setOnItemClickListener(this);
        searchDrug();
        mAdapter.notifyDataSetChanged();
    }
    @Override
    public void onClick(View v) {

    }

    @Override
    public void setOnItemClickListener(int i) {
        Intent intent = new Intent(this, detail_tips.class);
        intent.putExtra("data", mList.get(i));
        startActivity(intent);
    }
    //tìm kiếm thuốc theo mã tên và laoij
    private void searchDrug() {
        this.mList.clear();
        Cursor cursor = mDb.rawQuery("Select * From tips_table", null);
        if (cursor.moveToFirst()) {
            do {
                mList.add(new tips(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2)));
            } while (cursor.moveToNext());
        }
    }
}
