package com.example.drug.view.activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.drug.DataBase.DataBase;
import com.example.drug.R;
import com.example.drug.model.Drug;
import com.example.drug.utils.Condition;
import com.squareup.picasso.Picasso;

public class InforDrugActivity extends AppCompatActivity {

    private Drug mDrug;

    private Toolbar mToolbar;

    private ImageView mImg;

    private TextView mTvTen;

    private TextView mTvInfor;
    private WebView wv;

    private SQLiteDatabase mDb;
    public  Menu menu;

    //khởi tạo ban đầu
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infor_drug);
        mDrug = (Drug) getIntent().getSerializableExtra("data");
        init();
        setValue(mDrug);
    }

    //settupicon trên thánh toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.main, menu);
        menu = menu;

        if (checkAddMedicine()) {
            menu.findItem(R.id.item_scan).setIcon(R.drawable.start_en);
        }
        else {
            menu.findItem(R.id.item_scan).setIcon(R.drawable.star);
        }

        return true;

    }
    //sự kiện click phím trên thanh toolbar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        //click vào thêm dữ liệu vào bảng
        if (id == R.id.item_scan) {
            addMedicine();
            return true;
        }
        if (id == R.id.item_share) {
            shareText();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void shareText()
    {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = getText(mDrug);
        String shareSub = "Your subject here";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mDrug.getImage()+"\n"+shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }
    //khởi tạo dữ liệu ban đầu
    private void init() {
        this.mDb = DataBase.initDatabase(this, Condition.DATABASE_NAME);//khởi tạo kết nối sqlite
        mToolbar = findViewById(R.id.toolbar_infor);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("Chi tiết");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        //ánh xạ
        mImg = findViewById(R.id.img_detail);
        wv = findViewById(R.id.wv);

    }
    //thêm thuốc vào tủ thuốc hoặc xóa khỏi tủ thuốc
    public void addMedicine()
    {
        if (!checkAddMedicine()) {
            String ROW1 = "INSERT INTO tbl_medicine (code,name_drug,image) Values ('" + mDrug.getCode() + "', '" + mDrug.getNameDrug() + "', '" + mDrug.getImage() + "')";
            mDb.execSQL(ROW1);
            changeIcon();
        }else {
            String ROW1 = "DELETE FROM tbl_medicine WHERE code like '"+mDrug.getCode()+"%'";
            mDb.execSQL(ROW1);
            changeIcon();
        }
    }
    //check xem có bên trong bảng tử thuốc chưa
    public boolean checkAddMedicine()
    {
        Cursor cursor = mDb.rawQuery("Select count(*) From tbl_medicine where code like '" + mDrug.getCode() + "%'", null);
        cursor.moveToFirst();
        int count= cursor.getInt(0);
        cursor.close();
        if (count > 0)
        {
            return true;
        }
        return false;
    }
    //thay đổi icon của phím
    public void changeIcon(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (menu != null) {
                    MenuItem item = menu.findItem(R.id.item_scan);
                    if (item != null) {
                        if (checkAddMedicine()) {
                            item.setIcon(R.drawable.start_en);
                        }
                        else {
                            item.setIcon(R.drawable.star);
                        }

                    }
                }
            }
        });
    }
    //set sữ liêk để hiển thị
    private String getText(final Drug drug) {
        Picasso.with(this).load(drug.getImage()).placeholder(R.drawable.iconfinder_picture_1814111).into(mImg);
        String s = "";
        s += (drug.getNameDrug() == ""?"Đang cập nhật": drug.getNameDrug()) +"\n\n";
        s += "- Giá tiền: " + (drug.getPrice() == ""?"Đang cập nhật": drug.getPrice()+"VND") +"\n\n";
        s += drug.getIngredient() == ""?"": "- Thành phần: " + drug.getPrice() +"\n\n";
        s += drug.getUses() == ""?"":"- Công dụng: " + drug.getUses() +"\n\n";
        s += drug.getDosageAndUsage()==""?"":"- Liều dùng và cách dùng:" + drug.getDosageAndUsage() +"\n\n";
        s += drug.getAssignMedication()==""?"":"- Chỉ định: </font>" + drug.getAssignMedication() +"\n\n";
        s += drug.getNotAssignMedication()==""?"":"- Chống chỉ định: " + drug.getNotAssignMedication() +"\n\n";
        s += drug.getDosageForms()==""?"":"- Dạng bào chế: " + drug.getDosageForms() +"\n\n";
        s += drug.getPack()==""?"":"- Đóng gói: " + drug.getPack() +"\n\n";
        s += drug.getPharmacodynamic()==""?"": "- Dược lực học: " + drug.getPharmacodynamic() +"\n\n";
        s += drug.getPharmacokinetics() == ""?"":"- Dược động học: " + drug.getPharmacokinetics() +"\n\n";
        s +=  drug.getDrugInteractions() == ""?"":"- Tương tác thuốc: " + drug.getDrugInteractions() +"\n\n";
        s += drug.getNature()==""?"":"- Tính chất: " + drug.getNature() +"\n\n";
        s += drug.getOverdoseAndHandling()==""?"":"- Quá liều và cách xử lý: " + drug.getOverdoseAndHandling() +"\n\n";
        s +=  drug.getWarning()==""?"":"- Cảnh Báo: " + drug.getWarning() +"\n\n";

      return s;
    }
    private void setValue(final Drug drug) {
        Picasso.with(this).load(drug.getImage()).placeholder(R.drawable.iconfinder_picture_1814111).into(mImg);
        String s = "";
        s += "<h2> <font color='#2E64FE'>" + (drug.getNameDrug() == ""?"Đang cập nhật": drug.getNameDrug()) +"</font></h2>";
        s += "<h3><font color='red' >- Giá tiền: " + (drug.getPrice() == ""?"Đang cập nhật": drug.getPrice()+"VND") +"</font></h3>";
        s += drug.getIngredient() == ""?"": "<h3><font color='#01A9DB' >- Thành phần: </font></h3><h4><font color='#848484' >\"" + drug.getPrice() +"<h4>";
        s += drug.getUses() == ""?"":"<h3><font color='#01A9DB' >- Công dụng: </font></h3><h4><font color='#848484' >" + drug.getUses() +"</font></h4>";
        s += drug.getDosageAndUsage()==""?"":"<h3><font color='#01A9DB' >- Liều dùng và cách dùng: </font></h3><h4><font color='#848484' >" + drug.getDosageAndUsage() +"</><h4>";
        s += drug.getAssignMedication()==""?"":"<h3><font color='#01A9DB' >- Chỉ định: </font></h3><h4><font color='#848484' >" + drug.getAssignMedication() +"<h4>";
        s += drug.getNotAssignMedication()==""?"":"<h3><font color='#01A9DB' >- Chống chỉ định: </font></h3><h4><font color='#848484' >" + drug.getNotAssignMedication() +"<h4>";
        s += drug.getDosageForms()==""?"":"<h3><font color='#01A9DB' >- Dạng bào chế: </font></h3><h4><font color='#848484' >" + drug.getDosageForms() +"<h4>";
        s += drug.getPack()==""?"":"<h3><font color='#01A9DB' >- Đóng gói: </font></h3><h4><font color='#848484' >" + drug.getPack() +"<h4>";
        s += drug.getPharmacodynamic()==""?"": "<h3><font color='#01A9DB' >- Dược lực học: </font></h3><h4><font color='#848484' >" + drug.getPharmacodynamic() +"<h4>";
        s += drug.getPharmacokinetics() == ""?"":"<h3><font color='#01A9DB' >- Dược động học: </font></h3><h4><font color='#848484' >" + drug.getPharmacokinetics() +"<h4>";
        s +=  drug.getDrugInteractions() == ""?"":"<h3><font color='#01A9DB' >- Tương tác thuốc: </font></h3><h4><font color='#848484' >" + drug.getDrugInteractions() +"<h4>";
        s += drug.getNature()==""?"":"<h3><font color='#01A9DB' >- Tính chất: </font></h3><h4><font color='#848484' >" + drug.getNature() +"<h4>";
        s += drug.getOverdoseAndHandling()==""?"":"<h3><font color='#01A9DB' >- Quá liều và cách xử lý: </font></h3><h4><font color='#848484' >" + drug.getOverdoseAndHandling() +"<h4>";
        s +=  drug.getWarning()==""?"":"<h3><font color='#01A9DB' >- Cảnh Báo: </font></h3><h4><font color='#848484' >" + drug.getWarning() +"<h4>";

        wv.loadData(s, "text/html", "UTF-8");
        //  mTvInfor.setText(s);
    }
}
