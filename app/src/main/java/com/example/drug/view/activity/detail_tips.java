package com.example.drug.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

import com.example.drug.R;
import com.example.drug.model.tips;

import java.util.List;

public class detail_tips extends AppCompatActivity {
    public WebView webView;
    public tips tips;
    private Toolbar mToolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_detail);
        webView = findViewById(R.id.wv_tips);

        mToolbar = findViewById(R.id.toolbar_infor);
        setSupportActionBar(mToolbar);
        tips = (tips) getIntent().getSerializableExtra("data");
        getSupportActionBar().setTitle(tips.getName());
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        if (tips != null) {
            String s = "";
            s += tips.getText() == ""?"": "<h4><font color='#848484' >" + tips.getText() +"</font></h4>";
            webView.loadData(s, "text/html", "UTF-8");
        }
        else
        {

        }
    }
}
