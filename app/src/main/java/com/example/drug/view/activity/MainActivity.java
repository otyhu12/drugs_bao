package com.example.drug.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.example.drug.DataBase.DataBase;
import com.example.drug.DataBase.ProcessDataBase;
import com.example.drug.R;
import com.example.drug.model.Drug;
import com.example.drug.utils.Condition;
import com.example.drug.view.dialog.ConfrimDialog;
import com.example.drug.view.fragment.Fragment_Drug;
import com.example.drug.view.fragment.Fragment_Home;
import com.example.drug.view.fragment.OnChangeFragment;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        ConfrimDialog.OnAceptListener, OnChangeFragment, ProcessDataBase {

    private Toolbar mToolbar;
    private DrawerLayout mDrawer;

    private NavigationView mNavi;

    private Fragment mFragment;

    private ViewFlipper mViewFlipper;

    private Button mBtnExit;

    private LinearLayout mLayoutHome;

    private LinearLayout mLayoutDrug;

    private LinearLayout mLayoutCheck;

    private LinearLayout mLayoutVersion;

    private LinearLayout mLayoutInfor;

    private static MainActivity INSTANCE;

    public static List<Drug> mList = new ArrayList<>();

    public static MainActivity getInstance() {
        if (INSTANCE == null)
            INSTANCE = new MainActivity();
        return INSTANCE;
    }

    private SQLiteDatabase mDb;
    private ProcessDataBase mProcessDataBase;

    /*

     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        mDb = DataBase.initDatabase(this, Condition.DATABASE_NAME);

        mProcessDataBase = (ProcessDataBase) this;
        setProcessDataBase(this);
        mProcessDataBase.getListdrug();
        mFragment = new Fragment_Home();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.frame_content, mFragment).commit();

    }

    private void setProcessDataBase(ProcessDataBase mProcessDataBase) {
        this.mProcessDataBase = mProcessDataBase;
    }

    private void initView() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Tra cứu thuốc");
        mDrawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.no, R.string.no);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();
        mNavi = findViewById(R.id.nav_view);
        mViewFlipper = mNavi.findViewById(R.id.viewFilper_navi_left);
        for (int i = 0; i < Condition.IMAGE.length; i++) {
            //  This will create dynamic image view and add them to ViewFlipper
            setFlipperImage(Condition.IMAGE[i]);
        }

        mBtnExit = mNavi.findViewById(R.id.btn_exit);
        mBtnExit.setOnClickListener(this);

        mLayoutHome = mNavi.findViewById(R.id.layout_home);
        mLayoutHome.setOnClickListener(this);

        mLayoutDrug = mNavi.findViewById(R.id.layout_drug);
        mLayoutDrug.setOnClickListener(this);

        mLayoutCheck = mNavi.findViewById(R.id.layout_check);
        mLayoutCheck.setOnClickListener(this);

        mLayoutVersion = mNavi.findViewById(R.id.layout_version);
        mLayoutVersion.setOnClickListener(this);

        mLayoutInfor = mNavi.findViewById(R.id.layout_admin);
        mLayoutInfor.setOnClickListener(this);
    }

    private void setFlipperImage(int res) {
        ImageView image = new ImageView(this);
        image.setBackgroundResource(res);
        mViewFlipper.addView(image);
        mViewFlipper.setFlipInterval(Condition.TIME_TRANLATE_IMAGE);
        mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.out_from_left));
        mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.in_from_right));
        mViewFlipper.setAutoStart(true);
        mViewFlipper.startFlipping();
    }


    @Override
    public void onBackPressed() {
        ConfrimDialog dialog = null;
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        dialog = new ConfrimDialog(this);
        dialog.setOnAceptListener(this);
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.item_scan) {
            startActivity(new Intent(MainActivity.this, ScanCodeActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_exit:
                ConfrimDialog dialog = new ConfrimDialog(this);
                dialog.setOnAceptListener(this);
                dialog.show();
                break;
            case R.id.layout_admin:
                showSnackbar(mLayoutInfor, "Developer by Tieu Bao", 2000);
                closeDrawer();
                break;
            case R.id.layout_version:
                Cursor mCount= mDb.rawQuery("select count(*) from drugs", null);
                mCount.moveToFirst();
                int count= mCount.getInt(0);
                mCount.close();
                showSnackbar(mLayoutInfor, "Version : 1.0, " + "Dữ liêu thuốc: "+count, 2000);
                closeDrawer();
                break;
            case R.id.layout_home:
                changeFragment( new Fragment_Home());
                closeDrawer();
                break;
            case R.id.layout_drug:
                if (!(mFragment instanceof Fragment_Drug)) {
                    mFragment = new Fragment_Drug();
                    changeFragment(mFragment);
                }
                closeDrawer();
                break;
            case R.id.layout_check:
                startActivity(new Intent(MainActivity.this, ScanCodeActivity.class));
                closeDrawer();
                break;
        }
    }

    public void showSnackbar(View view, String message, int duration) {
        // Create snackbar
        final Snackbar snackbar = Snackbar.make(view, message, duration);

        // Set an action on it, and a handler
        snackbar.setAction("DISMISS", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });

        snackbar.show();
    }

    /**
     * @param fragment
     */
    private void changeFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().setAllowOptimization(true).
                replace(R.id.frame_content, fragment, fragment.getClass().getName()).
                addToBackStack(fragment.getClass().getName()).commit();
    }

    private void closeDrawer() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void setOnAceptListener() {
        finish();
    }

    @Override
    public void setOnChangeFragment() {

            changeFragment(new Fragment_Drug());

    }
    //lấy ra list thuốc
    @Override
    public void getListdrug() {
        Cursor cursor = mDb.rawQuery("Select * From drugs", null);
        if (cursor.moveToFirst()) {
            do {
                mList.add(new Drug(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getString(3),
                        cursor.getString(4),
                        cursor.getString(5),
                        cursor.getString(6),
                        cursor.getString(7),
                        cursor.getString(8),
                        cursor.getString(9),
                        cursor.getString(10),
                        cursor.getString(11),
                        cursor.getString(12),
                        cursor.getString(13),
                        cursor.getString(14),
                        cursor.getString(15),
                        cursor.getString(16),
                        cursor.getString(17),
                        cursor.getString(18)));
            } while (cursor.moveToNext());
        }
    }
}
