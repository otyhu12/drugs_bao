package com.example.drug.model;

import java.io.Serializable;

public class Drug implements Serializable {


    private String code;
    private String nameDrug;
    private String ingredient;
    private String dosageAndUsage;
    private String warning;
    private String image;
    private String assignMedication;
    private String notAssignMedication;
    private String dosageForms;
    private String pack;
    private String pharmacodynamic;
    private String pharmacokinetics;
    private String unwantedEffects;
    private String overdoseAndHandling;
    private String drugInteractions;
    private String note;
    private String nature;
    private String uses;
    private String price;

    public Drug(String code, String nameDrug, String ingredient, String dosageAndUsage, String warning, String image, String assignMedication, String notAssignMedication, String dosageForms, String pack, String pharmacodynamic, String pharmacokinetics, String unwantedEffects, String overdoseAndHandling, String drugInteractions, String note, String nature, String uses, String price) {
        this.code = code;
        this.nameDrug = nameDrug;
        this.ingredient = ingredient;
        this.dosageAndUsage = dosageAndUsage;
        this.warning = warning;
        this.image = image;
        this.assignMedication = assignMedication;
        this.notAssignMedication = notAssignMedication;
        this.dosageForms = dosageForms;
        this.pack = pack;
        this.pharmacodynamic = pharmacodynamic;
        this.pharmacokinetics = pharmacokinetics;
        this.unwantedEffects = unwantedEffects;
        this.overdoseAndHandling = overdoseAndHandling;
        this.drugInteractions = drugInteractions;
        this.note = note;
        this.nature = nature;
        this.uses = uses;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameDrug() {
        return nameDrug;
    }

    public void setNameDrug(String nameDrug) {
        this.nameDrug = nameDrug;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getDosageAndUsage() {
        return dosageAndUsage;
    }

    public void setDosageAndUsage(String dosageAndUsage) {
        this.dosageAndUsage = dosageAndUsage;
    }

    public String getWarning() {
        return warning;
    }

    public void setWarning(String warning) {
        this.warning = warning;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAssignMedication() {
        return assignMedication;
    }

    public void setAssignMedication(String assignMedication) {
        this.assignMedication = assignMedication;
    }

    public String getNotAssignMedication() {
        return notAssignMedication;
    }

    public void setNotAssignMedication(String notAssignMedication) {
        this.notAssignMedication = notAssignMedication;
    }

    public String getDosageForms() {
        return dosageForms;
    }

    public void setDosageForms(String dosageForms) {
        this.dosageForms = dosageForms;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getPharmacodynamic() {
        return pharmacodynamic;
    }

    public void setPharmacodynamic(String pharmacodynamic) {
        this.pharmacodynamic = pharmacodynamic;
    }

    public String getPharmacokinetics() {
        return pharmacokinetics;
    }

    public void setPharmacokinetics(String pharmacokinetics) {
        this.pharmacokinetics = pharmacokinetics;
    }

    public String getUnwantedEffects() {
        return unwantedEffects;
    }

    public void setUnwantedEffects(String unwantedEffects) {
        this.unwantedEffects = unwantedEffects;
    }

    public String getOverdoseAndHandling() {
        return overdoseAndHandling;
    }

    public void setOverdoseAndHandling(String overdoseAndHandling) {
        this.overdoseAndHandling = overdoseAndHandling;
    }

    public String getDrugInteractions() {
        return drugInteractions;
    }

    public void setDrugInteractions(String drugInteractions) {
        this.drugInteractions = drugInteractions;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getUses() {
        return uses;
    }

    public void setUses(String uses) {
        this.uses = uses;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}