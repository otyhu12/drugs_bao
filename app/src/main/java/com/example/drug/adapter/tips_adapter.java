package com.example.drug.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.drug.R;
import com.example.drug.model.Drug;
import com.example.drug.model.tips;
import com.example.drug.view.activity.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class tips_adapter extends RecyclerView.Adapter<tips_adapter.tipViewHolder> {

    private Context mContext;

    private List<tips> mList;//chứa list thuốc

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public tips_adapter(Context mContext, List<tips> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public tips_adapter.tipViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_tips, viewGroup, false);
        return new tips_adapter.tipViewHolder(view);
    }

    //set dữ liệu cho từng item trong list
    @Override
    public void onBindViewHolder(@NonNull tips_adapter.tipViewHolder tipViewHolder, final int i) {
        tips drug = mList.get(i);
        tipViewHolder.txt_tips.setText(drug.name + "");
        tipViewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.setOnItemClickListener(i);
            }
        });
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class tipViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mLayout;
        private ImageView mImg;
        private TextView txt_tips;

        public tipViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.layout_itemtips);
            txt_tips = itemView.findViewById(R.id.txt_tips);
        }
    }
}
