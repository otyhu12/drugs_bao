package com.example.drug.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.drug.R;
import com.example.drug.model.Drug;
import com.example.drug.view.activity.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DrugAdapter extends RecyclerView.Adapter<DrugAdapter.DrugViewHolder> {

    private Context mContext;

    private List<Drug> mList;//chứa list thuốc

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public DrugAdapter(Context mContext, List<Drug> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public DrugViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_lits_drug, viewGroup, false);
        return new DrugViewHolder(view);
    }

    //set dữ liệu cho từng item trong lí
    @Override
    public void onBindViewHolder(@NonNull DrugViewHolder drugViewHolder, final int i) {
        Drug drug = mList.get(i);
        drugViewHolder.mTv.setText(drug.getNameDrug() + "");
        Picasso.with(mContext).load(drug.getImage()).into(drugViewHolder.mImg);
        drugViewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.setOnItemClickListener(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class DrugViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mLayout;
        private ImageView mImg;
        private TextView mTv;

        public DrugViewHolder(@NonNull View itemView) {
            super(itemView);
            mLayout = itemView.findViewById(R.id.item_layout);
            mImg = itemView.findViewById(R.id.item_img_anh);
            mTv = itemView.findViewById(R.id.item_title);
        }
    }
}
